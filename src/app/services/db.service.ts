import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  constructor(public db: AngularFireDatabase, public http: HttpClient) { }

  getCategories() {
    return this.db.list(`categories`).valueChanges();
  }

  getExercise(id: string): Promise<any> {
    let ref = this.db.database.ref(`exercises/${id}`)

    return new Promise((res, err) => {
      ref.on('value', (snapshot) => {
        if (snapshot.exists()) {
          res(snapshot.val())
        } else {
          err(`Some error`)
        }
      });
    });

  }

  HOST = `https://72daee1f.ngrok.io/query`
  executeQuery(q: string): Observable<any> {
    return this.http.post(this.HOST, {
      operation: 'execute',
      query: q
    })
  }

}
