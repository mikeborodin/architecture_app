import { Component, OnInit } from '@angular/core';
import { DbService } from 'src/app/services/db.service'

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  constructor(
    private databaseService: DbService
  ) { }

  ngOnInit() {
  }

  appitems = [
    {
      label: 'SQL Select',
      icon: 'select_all',
      items: [
        {
          label: 'exercise 1',
          link: '/item-1-1',
        },
        {
          label: 'exercise 2',
          link: '/item-1-1',
        },
        {
          label: 'exercise 3',
          link: '/item-1-1',
        }
      ]
    },
    {
      label: 'SQL Order By',
      icon: 'sort',
      items: [
        {
          label: 'exercise 1',
          link: '/item-1-1',
        },
        {
          label: 'exercise 2',
          link: '/item-1-1',
        }
      ]
    },
    {
      label: 'SQL where',
      items: [
        {
          label: 'exercise 1',
          link: '/item-1-1',
        },
        {
          label: 'exercise 2',
          link: '/item-1-1',
        }
      ]
    },
    {
      label: 'SQL Delete',
      icon: 'delete',
      items: [
        {
          label: 'exercise 1',
          link: '/item-1-1',
        },
        {
          label: 'exercise 2',
          link: '/item-1-1',
        },
        {
          label: 'exercise 3',
          link: '/item-1-1',
        }
      ]
    },
    {
      label: 'SQL Insert',
      items: [
        {
          label: 'exercise 1',
          link: '/item-1-1',
        }
      ]
    }
  ];

  config = {
    paddingAtStart: true,
    classname: 'my-custom-class',
    listBackgroundColor: '#42425C',
    fontColor: ' rgba(255, 255, 255, 0.64',
    backgroundColor: '#42425C',
    selectedListFontColor: '#d392ff',
  };

}
