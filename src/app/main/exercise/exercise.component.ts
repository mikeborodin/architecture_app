import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbService } from '../../services/db.service';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent implements OnInit {


  isAnswerShown:boolean = false
  query: string = ""
  results: any[] = [
    { name: 'New yurk!' },
    { name: 'New yurk!' },
  ]

  exercise: any = {
    task: 'Select all...',
    etalon: 'SELECT *',
    nextlink: 'ex2'
  }

  constructor(private ar: ActivatedRoute,
    private databaseService: DbService) {


     }

  ngOnInit() {
    this.ar.params.subscribe(
      params => {
        this.isAnswerShown = false;
        console.log(params);
        this.databaseService.getExercise(params['id'])
        .then(ex=>{
          console.log(`Got ex: ${ex}`);
          
          this.exercise = ex;          
        }).catch(console.error)
      })
  }


  onQuerySubmit() {
    if(this.query.length!=0){
      this.databaseService.executeQuery(this.query).subscribe((res)=>{
        console.log(`Res ${res}`);
        this.results = res;
        })
    
    }else{
      alert("Type your query please")
    }    
    
  }

}
